#!/bin/bash

export PROLOG_PATH="$LANG_PATH/dist"

################################################################################

ronin_require_prolog () {
    echo hello world >/dev/null
}

ronin_include_prolog () {
    motd_text "    -> Prolog  : "$PROLOG_PATH
}

################################################################################

ronin_setup_prolog () {
    echo hello world >/dev/null
}

